<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $superheroes = App\superhero::paginate(5);
    return view('welcome', compact('superheroes'));
});
Route::get('/deleteSuperHero/{superhero}', 'SuperheroController@deleteSuperHero');
Route::get('/editsuperhero/{superhero}' , 'SuperheroController@edit');
Route::get('/showsuperhero/{superhero}', 'SuperheroController@show');
Route::get('/deleteimage/{id}' , 'SuperheroController@deleteImage');

Route::post('/saveSuperhero', 'SuperheroController@storeSuperHero');
