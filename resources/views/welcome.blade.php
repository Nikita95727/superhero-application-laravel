<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Superheroes</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/superhero.css')}}">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/ae7e200d98.js"></script>
</head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (session()->exists('success'))
                <div class="alert alert-success" role="alert">
                    {{session()->get('success')}}
                </div>
            @endif
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">{{__('Image')}}</th>
                                    <th scope="col">{{__('Nickname')}}</th>
                                    <th scope="col">{{__('Actions')}}</th>
                                </tr>
                                </thead>
                                @foreach ($superheroes as $superhero)
                                    <tr>
                                        <td width="10%">
                                            @if ($superhero->getImage())
                                                <a href = "{{URL::to('/showsuperhero/'.$superhero->id)}}" title="Show description">
                                                    <img class="table-superhero-image" src="{{$superhero->getImage()}}">
                                                </a>
                                            @endif
                                        </td>
                                        <td width="80%">
                                            <a href = "{{URL::to('/showsuperhero/'.$superhero->id)}}" title="Show description">
                                                {{$superhero->nickname}}
                                            </a>
                                        </td>
                                        <td width="10%">
                                            <a href="{{URL::to('/deleteSuperHero/'.$superhero->id)}}" title="Delete superhero">
                                                <i class="fas fa-trash"></i>
                                            </a>
                                            <a href="{{URL::to('/editsuperhero/'.$superhero->id)}}" title="Edit superhero">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <button class="btn btn-info" data-toggle="modal" data-target="#createSuperHeroModal">
                                        <i class="fas fa-plus" title="{{__('Add superhero')}}">
                                        </i>
                                        <span>Add new superhero</span>
                                    </button>
                                </tr>
                            </table>
                        </div>
                    </div>
                    {{ $superheroes->links() }}
                    <!-- Modal -->
                    <div class="modal fade" id="createSuperHeroModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <form method="post" action="{{URL::to('/saveSuperhero')}}">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">{{__('Add new superhero')}}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body create-super-hero-form">
                                        <label for="superHeroNickname">{{__('Nickname')}}</label>
                                        <input type="text" class="form-control" name="superHeroNickname">
                                        <label for="superHeroRealName">{{__('Real name')}}</label>
                                        <input type="text" class="form-control" name="superHeroRealName">
                                        <label for="superHeroOriginDescription">{{__('Origin description')}}</label>
                                        <textarea class="form-control" name="superHeroOriginDescription"></textarea>
                                        <label for="superHeroSuperPowers">{{__('Super powers')}}</label>
                                        <input type="text" class="form-control" name="superHeroSuperPowers">
                                        <label for="superHeroCatchPhrase">{{__('Catch phrase')}}</label>
                                        <input type="text" class="form-control" name="superHeroCatchPhrase">
                                        <a href="#" class="add-image">Add one more image</a>
                                        <div class="images-upload-input">
                                            <label for="superHeroImages">{{__('Image')}}</label>
                                            <input type="text" class="form-control" name="superHeroImages[]">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                                        <button type="submit" class="btn btn-primary">{{__('Save superhero')}}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <script>
                    $('.add-image').click(function(event) {
                        addDynamicExtraField();
                        return false;
                    });

                    function addDynamicExtraField() {
                        $('<label for = "superHeroImages">Image</label>').appendTo($('.create-super-hero-form'));
                        $('<input type="text" name="superHeroImages[]" class="form-control">').appendTo($('.create-super-hero-form'));
                    }
                </script>
            </div>
        </div>
    </body>
</html>
