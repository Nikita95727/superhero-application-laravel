<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{__('Superheros')}}</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/superhero.css')}}">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/ae7e200d98.js"></script>
</head>
<body>
    <div class="container">
        <form method="post" action="{{URL::to('/saveSuperhero')}}" class="superhero-edit-form">
            @if (session()->exists('success'))
                <div class="alert alert-success" role="alert">
                    {{session()->get('success')}}
                </div>
            @endif
            <input type="hidden" value="{{$superhero->id}}" name="superHeroId">
            <label for="superHeroNickname">{{__('Nickname')}}</label>
            <input type="text" class="form-control" name="superHeroNickname" value="{{$superhero->nickname}}" maxlength="100">
            <label for="superHeroRealName">{{__('Real name')}}</label>
            <input type="text" class="form-control" name="superHeroRealName" value="{{$superhero->real_name}}" maxlength="100">
            <label for="superHeroOriginDescription">{{__('Origin description')}}</label>
            <textarea class="form-control" name="superHeroOriginDescription">
                {{$superhero->origin_description}}
            </textarea>
            <label for="superHeroSuperPowers">{{__('Super powers')}}</label>
            <input type="text" class="form-control" name="superHeroSuperPowers" value="{{$superhero->superpowers}}" maxlength="255">
            <label for="superHeroCatchPhrase">{{__('Catch phrase')}}</label>
            <input type="text" class="form-control" name="superHeroCatchPhrase" value="{{$superhero->catch_phase}}" maxlength="255">
            <a href="#" class="add-image">Add one more image</a>
            <div class="images-upload-inputs"></div>
            <div class="superhero-galery">
                <div class="row">
                @foreach ($superhero->getImages() as $image)
                    <div class="col-3">
                        <div class="card">
                            <a href="{{URL::to('/deleteimage/'.$image['id'])}}"><i class="fas fa-times superhero-image-delete"></i></a>
                            <img src="{{$image['image']}}" class="card-img-top">
                        </div>
                    </div>
                @endforeach
                </div>
            </div>
            <button type="submit" class="btn btn-success">{{__('Edit')}}</button>
        </form>
    </div>
    <script>
        $('.add-image').click(function(event) {
            addDynamicExtraField();
            return false;
        });

        function addDynamicExtraField() {
            $('<label for = "superHeroImages">Image</label>').appendTo($('.images-upload-inputs'));
            $('<input type="text" name="superHeroImages[]" class="form-control">').appendTo($('.images-upload-inputs'));
        }
    </script>
</body>
</html>
