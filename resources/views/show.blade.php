<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{__('Superheros')}}</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/superhero.css')}}">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/ae7e200d98.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-5 col-md-9 col-lg-9">
            <h3>{{$superhero->nickname}}</h3>
            <div class="superhero-origin-description">
                <span>{{$superhero->origin_description}}</span>
            </div>
        </div>
        <div class="col-sm-4 col-md-3 col-lg-3">
            <div class="superhero-image">
                <img src="{{$superhero->getImage()}}">
            </div>
            <div class="superhero-real-name">
                <span>{{__('Real name: '.$superhero->real_name)}}</span>
            </div>
            <div class="superhero-catch-phrases">
                <span>{{__('Catch phrases: '.$superhero->catch_phase)}}</span>
            </div>
        </div>
    </div>
    <div class="superhero-photo-galery">
        <div class="superhero-galery-header">
            <h3>{{__('Superhero galery')}}</h3>
        </div>
        @foreach($superhero->getImages() as $image)
            <img class="show-superhero-image" src="{{$image['image']}}">
        @endforeach
    </div>
</div>
</body>
</html>
