<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class SuperheroController extends Controller
{
    public function storeSuperHero(Request $request)
    {
        $this->validate($request, [
           'superHeroNickname' => 'required',
            'superHeroRealName' => 'required',
            'superHeroOriginDescription' => 'required',
            'superHeroSuperPowers' => 'required',
            'superHeroCatchPhrase' => 'required',
        ]);

        if ($request->input('superHeroId')) {
            $id = $request->input('superHeroId');

            if ($id) {
                $superhero = App\superhero::find($id);
                $this->_setSuperheroAndImage($superhero, $request);

                return redirect()->back()->with('success', 'Superhero had been edited successfully');
            }
        }

        $superhero = new App\superhero;
        $this->_setSuperheroAndImage($superhero, $request);
        return redirect('/')->with('success','Superhero had been added successfully');
    }

    public function deleteSuperHero($id)
    {
        App\superhero::find($id)
            ->delete();
        return redirect('/')->with('success','Superhero had been deleted successfully');
    }

    public function edit($id)
    {
        $superhero = App\superhero::find($id);

        return view('edit', compact('superhero'));
    }

    public function show($id)
    {
        $superhero = App\superhero::find($id);

        return view('show', compact('superhero'));
    }

    public function deleteImage($id)
    {
        App\image::find($id)
            ->delete();

        return redirect()->back();
    }

    private function _setSuperheroAndImage($superhero, $request)
    {
        $superhero->nickname = $request->input('superHeroNickname');
        $superhero->real_name = $request->input('superHeroRealName');
        $superhero->origin_description = $request->input('superHeroOriginDescription');
        $superhero->superpowers = $request->input('superHeroSuperPowers');
        $superhero->catch_phase = $request->input('superHeroCatchPhrase');
        $superhero->save();

        if ($request->input('superHeroImages')) {
            $superHeroImages = $request->input('superHeroImages');
            foreach ($superHeroImages as $superHeroImage){
                $image = new App\image;
                $image->image = $superHeroImage;
                $image->superhero_id = $superhero->id;
                $image->save();
            }
        }
    }
}
