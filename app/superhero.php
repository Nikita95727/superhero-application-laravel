<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class superhero extends Model
{
    public function getImage()
    {
        $image = image::where('superhero_id', $this->id)->get();

        return $image[0]['image'];
    }

    public function getImages()
    {
        $images = image::where('superhero_id', $this->id)->get();
        return $images;
    }
}
