How to expand web-application:
1. Go to .env and correct the database access: enter the database name, database user and password;
2. Go to config/database and write the database accesses again:
database name, database user and password.
3. Enter the command in the root of the web-site folder: 
php artisan migrate. It will expand all the tables of database.

On the main page you can see the button "Add new superhero" and the table, where all superhero are displaying
How to use:
If you want to add the new superhero, press the button "Add new superhero", then will be opened the popup, where you can write all the fields. You can enter only the link of image.
If you want to open the detailed page, you should press on the image or the nickname of the hero. 
if you want to delete the superhero, just press on icon of the trash, if you want edit the superhero, press on the icon 'Edit'.

On the superhero detailed page, you can see the superhero name, real name, avatar, description and the galery of superhero images.

On the superhero edit page, you can edit every field and add new image and or delete useless one.
If you want to edit the field, just write on it's own field the new information and the press 'Edit'
If you want to delete the useless image, just hover on the image and then you will see the icon 'X', just press it and the useless image will be deleted.

Thank you for using of my application!

